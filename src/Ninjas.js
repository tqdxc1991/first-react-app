import React from 'react'

const Ninjas =(props)=>{
    // define the props, one object
    //const { name, age, belt } = this.props;

    //integrate the props into JSX
    // return (
    //   <div className="ninja">
    //     <div>Name: { name }</div>
    //     <div>Age: { age }</div>
    //     <div>Belt: { belt }</div>
    //   </div>
    // )

    //define the props , a list of objects
    const { ninjas } = this.props;

    // integrate the props into a list of JSX
    const ninjaList = ninjas.map(ninja => {
      return (
        <div className="ninja" key={ninja.id}>
          <div>Name: { ninja.name }</div>
          <div>Age: { ninja.age }</div>
          <div>Belt: { ninja.belt }</div>
        </div>
      )
    });

    return (
      <div className="ninja-list">
        { ninjaList }
      </div>
    )
  
}

export default Ninjas;
