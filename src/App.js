import React, {Component} from 'react';
import Ninjas from './Ninjas';

class App extends Component {
  //state = { name: 'Qiong', age: 30, belt: 'black'}
  state = {
    ninjas: [
      { name: 'Ryu', age: 30, belt: 'black', id: 1 },
      { name: 'Yoshi', age: 20, belt: 'green', id: 2 },
      { name: 'Crystal', age: 25, belt: 'pink', id: 3 }
    ]
  }
  handleClick = (e) => {
    console.log(this.state)
  }

  changeName = (e) => {
    this.setState ({ name : e.target.value})
  }
  
  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.name)
  }

  render(){

    return (
      <div className="App">
        <h1>coucou react</h1>        
        {/* <p>show the Component with its props:</p>
        <Ninjas name="Qiong" age="29" belt="black" />

        <p>Welcome {this.state.name }</p>
        <form onSubmit ={this.handleSubmit}>
          <input onChange={this.changeName}></input>
          <button>Submit</button>
        </form>
        <button onClick={this.handleClick}> Click me</button> */}
        <Ninjas ninjas={this.state.ninjas}/>
      </div>
    );
  }
}

export default App;
